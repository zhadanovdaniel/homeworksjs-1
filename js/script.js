//1. Як можна оголосити змінну у Javascript?

// У Javascript змінну можна оголосити за допомогою ключового слова "var", "let" або "const". 
// Ключове слово "var" використовується для оголошення змінних з функціональним областю видимості,
//  а "let" та "const" - для оголошення блочних змінних з обмеженою областю видимості. Крім того, 
// "let" можна оновлювати, а "const" - ні.

// 2.У чому різниця між функцією prompt та функцією confirm?

// Основна різниця між ними полягає в тому, що prompt використовується для отримання введеного користувачем тексту, 
// тоді як confirm використовується для підтвердження дії користувача.

//3. Що таке неявне перетворення типів? Наведіть один приклад.

// Неявне перетворення типів (або автоматичне перетворення типів) - це процес, коли JavaScript автоматично перетворює значення з одного типу даних в інший без явного вказівника.

// Один з прикладів неявного перетворення типів в JavaScript полягає у використанні оператора "+" для з'єднання рядків та чисел.

// Завдання:


// Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.

let admin;
let myname = 'Daniel';
admin = myname; 
console.log(admin);

// Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.

let days = 4;
let secInDay =  86400;
let secConvert = days* secInDay;

// Запитайте у користувача якесь значення і виведіть його в консоль.

console.log(secConvert);
let userPrompt = prompt('Write something pls');
console.log(userPrompt);